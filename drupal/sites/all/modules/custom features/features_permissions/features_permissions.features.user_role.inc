<?php
/**
 * @file
 * features_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function features_permissions_user_default_roles() {
  $roles = array();

  // Exported role: journalist.
  $roles['journalist'] = array(
    'name' => 'journalist',
    'weight' => 3,
  );

  return $roles;
}
